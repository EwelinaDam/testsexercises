package com.sda;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class CalculatorTest {
    private static Calculator calculator;

    @BeforeClass
    public static void initializeCalculator (){
        calculator = new Calculator();
    }

    @Test
    public void shouldReturnTwoWhenSevenDividedByFive() {
       // jest BeforeClass, który nam inicjalizuje zmienną kalkulator
       Integer result = calculator.restFromDivide(7, 5);

        //then
        // sprawdza, czy dwa jest takie samo jak result, pierwszy parametr to komunikat
        // który wyświetli się, gdy test nie przechodzi
        Assert.assertEquals("test nie działą, błąd w zwracaniu reszty z dzielenia",
                Integer.valueOf(2), result);
    }

    @Test
    public void shouldReturnLargestValue1() {
        //given
        Calculator calculator = new Calculator(); //jeśli nie ma BeforeClass trzeba tutaj utworzyć obiekt
        //when
        Integer result = calculator.larrgestAbs(-2, 0, 1);
        //then
        Assert.assertEquals("test jest błędny, liczba nie jest największa", Integer.valueOf(2), result);
    }

    @Test
    public void shouldReturnLargestValue2() {
        //given
        Calculator calculator = new Calculator();
        //when
        Integer result = calculator.larrgestAbs(-2, -10, -1);
        //then
        Assert.assertEquals("test jest błędny, liczba nie jest największa", Integer.valueOf(10), result);
    }

    @Test
    public void shouldReturnLargestValue3() {
        //given
        Calculator calculator = new Calculator();
        //when
        Integer result = calculator.larrgestAbs(-2, 0, 15);
        //then
        Assert.assertEquals("test jest błędny, liczba nie jest największa", Integer.valueOf(15), result);
    }

    @Test
    public void shouldReturn1When5DivideBy5() {
        //given
        Calculator calculator = new Calculator();
        //when
        Integer result = calculator.divide(5, 5);
        //then
        Assert.assertEquals("test jest błędny, błędny wynik dzielenia", Integer.valueOf(1), result);
    }

    @Test(expected = ArithmeticException.class)
    public void shouldThrowArithmeticExceptionWhen5DividedBy0() {
        //given
        Calculator calculator = new Calculator();
        //when
        Integer result = calculator.divide(7, 0);

    }

    @Test
    public void shouldReturn5ForElements() {

        //given
        Calculator calculator = new Calculator();
        //when
        Integer result = calculator.maxFromList(Arrays.asList(1, 2, null, 5));
        //then
        Assert.assertEquals("test jest błędny, nie zwraca maxa", Integer.valueOf(5), result);

    }

    @Test
    public void shouldReturn0ForEmptyList() {

        //given
        Calculator calculator = new Calculator();
        //when
        Integer result = calculator.maxFromList(Collections.emptyList());
        //then
        Assert.assertEquals("test jest błędny, nie zwraca maxa", Integer.valueOf(0), result);

    }
    @Test
    public void shouldReturn5ForList() {

        //given
        Calculator calculator = new Calculator();
        //when
        Integer result = calculator.maxFromList(Arrays.asList(1,2,null,5));
        //then
        Assert.assertEquals("test jest błędny, nie zwraca maxa", Integer.valueOf(5), result);

    }

    @Test
    public void shouldReturn0ForNullList() {

        //given
        Calculator calculator = new Calculator();
        //when
        Integer result = calculator.maxFromList(null);
        //then
        Assert.assertEquals("test jest błędny, nie zwraca maxa", Integer.valueOf(0), result);

    }

    @Test
    public void shouldReturnAlphabeticalList() {
        //when
        List <String> result = calculator.sortAlphabetically("Tomek","Adam","Monika");
        //then
        Assert.assertEquals("test jest błędny",
                Arrays.asList("Adam","Monika","Tomek"), result);

    }

    @Test
    public void shouldReturnAlphabeticalListWithoutNull() {
        //when
        List <String> result = calculator.sortAlphabetically(null,"Adam","Monika");
        //then
        Assert.assertEquals("test jest błędny",
                Arrays.asList("Adam","Monika"), result);

    }

    @Test
    public void shouldReturnEmptyList() {
        //when
        List <String> result = calculator.sortAlphabetically(null,null,null);
        //then
        Assert.assertEquals("test jest błędny",
                Arrays.asList(), result);

    }
    @Test
    public void shouldReturnTrueForKajak() {
        //when
       Boolean result = calculator.isPalindrome("kajak");
        //then
        Assert.assertEquals("test jest błędny, wartość powinna być true", true, result);

    }
    @Test
    public void shouldReturnFalseForEwelina() {
        //when
        Boolean result = calculator.isPalindrome("ewelina");
        //then
        Assert.assertEquals("test jest błędny, wartość powinna być true", false, result);

    }
}