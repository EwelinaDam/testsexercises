package com.sda;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class PalindromeTest {

    @Test
    public void shouldReturnTrueWhenStringIsNull() {
        //given

        PalindromeHelper palindrome = new PalindromeHelper();

        //when
        boolean isPalindrome = palindrome.isPalindrome(null);

        Assert.assertTrue(isPalindrome);
    }

    @Test
    public void shouldReturnFalseWhenStringIsEmpty() {
        //given

        PalindromeHelper palindrome = new PalindromeHelper();

        //when
        boolean isPalindrome = palindrome.isPalindrome("");
        //then
        Assert.assertFalse(isPalindrome);
    }

    @Test
    @Parameters({"anna","kajak"})
    public void shouldReturnTrueForPalindrome(String str){
        //given

        PalindromeHelper palindrome = new PalindromeHelper();

        //when
        boolean isPalindrome = palindrome.isPalindrome(str);

        //then
        Assert.assertTrue(isPalindrome);

    }
    @Test
    @Parameters ({"leon", "donat"})
    public void shouldReturnFalseForNonPalindrome(String str) {
        //given

        PalindromeHelper palindrome = new PalindromeHelper();

        //when
        boolean isPalindrome = palindrome.isPalindrome(str);

        //then
        Assert.assertFalse(isPalindrome);

    }
}
