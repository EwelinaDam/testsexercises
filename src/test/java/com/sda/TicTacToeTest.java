package com.sda;

import com.tictac.Symbol;
import com.tictac.TicTacToe;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Array;

import static com.tictac.Symbol._;

public class TicTacToeTest {

    @Test
    public void shouldCreateEmptyArray() {
        //given
        TicTacToe ticTacToe = new TicTacToe();

        //when
        Symbol[] result = ticTacToe.createBoard();

        //then
        Assert.assertArrayEquals(new Symbol[9], result);
    }
    @Test
    public void shouldReturnCommentWhenBoxNumLargerThen9() {
        //given
        TicTacToe ticTacToe = new TicTacToe();

        //when
        Symbol[] result = ticTacToe.createBoard();

        //then
        Assert.assertArrayEquals(new Symbol[9], result);
    }
}
