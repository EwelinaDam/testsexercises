package com.sda;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class CalculatorTest2 {

    private Calculator calculator;

    @Before
    public void setUp() {
        calculator = new Calculator();
    }
@Test
    @Parameters({"8,5,3", "11,3,2"})
    public void shouldReturnCurrentRestFromDivision(int num, int divider, int expRest) {
        //when
        Integer result = calculator.restFromDivide(num, divider);
        //then
        Assert.assertEquals("Test nie przeszedł", Integer.valueOf(expRest), result);

    }
    @Test
    @Parameters({"null, false", "anna, true","kajak, true", "aaa, true"})
    public void shouldReturnTrueIfPalindrome(String str, Boolean info) {
        //when
        Boolean result = calculator.isPalindrome(str);
        //then
        Assert.assertEquals("Test nie przeszedł", info, result);

    }

}
