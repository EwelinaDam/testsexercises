package com.sda;

import org.junit.Assert;
import org.junit.Test;

public class FibonacciTest {
    @Test
    public void shouldReturnOneForFirstElement() {
        //given
        Fibonacci fibonacci = new Fibonacci();
        //when
        int result = fibonacci.fibonacciElement(1);
        //then
        Assert.assertEquals(1, result);
    }
    @Test
    public void shouldReturnOneForSecoundElement() {
        //given
        Fibonacci fibonacci = new Fibonacci();
        //when
        int result = fibonacci.fibonacciElement(1);
        //then
        Assert.assertEquals(1, result);
    }

    @Test
    public void shouldReturnTwoForThirdElemnt() {
        //given
        Fibonacci fibonacci = new Fibonacci();
        //when
        int result = fibonacci.fibonacciElement(3);
        //then
        Assert.assertEquals(2, result);
    }

    @Test
    public void shouldReturnThreeForFourthElement() {
        //given
        Fibonacci fibonacci = new Fibonacci();
        //when
        int result = fibonacci.fibonacciElement(4);
        //then
        Assert.assertEquals(3, result);
    }

}
