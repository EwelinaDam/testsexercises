package com.sda;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class HorsesTest {
    @Test
    public void shouldReturnNamesOfTheHorses() {
        //given
        HorsesInTheStable horses = new HorsesInTheStable();
        //when
        List<String> result = horses.getHorses();
        //then
        assertThat(result).hasSize(9)
                .contains("Castelli", "Humor")
                .doesNotContain("Bruno");
    }
}
