package com.sda;

public class PalindromeHelper {
    public boolean isPalindrome(String str) {
        if (str == null) {
            return true;
        }
        String[] split = str.split("");
        for (int i = 0; i < split.length / 2; i++) {
            if (split[i].equals(split[split.length - 1 - i])) {
                return true;
            }
        }
        return false;
    }
}