package com.sda;

import java.util.Arrays;
import java.util.List;

public class HorsesInTheStable {
    List<String> horses;

    public List<String> getHorses() {
        return Arrays.asList("Castelli", "Humor", "Demon", "Mustang",
                "Blacky", "Livia", "Molly", "Gaiger", "Checks");
    }
}
