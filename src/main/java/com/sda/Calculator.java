package com.sda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Calculator {


    public int restFromDivide(int num, int divider) {
        return num % divider;
    }

    // returns the largest absolute value

    public Integer larrgestAbs(Integer num1, Integer num2, Integer num3) {
        Integer absN1 = Math.abs(num1);
        Integer absN2 = Math.abs(num2);
        Integer absN3 = Math.abs(num3);
        return Math.max(Math.max(absN1, absN2), absN3);

    }

    public Integer divide(Integer num, Integer divider) {
        if (divider.equals(0)) {
            throw new ArithmeticException("Divider should not be zero");
        }
        return num / divider;
    }

    public Integer maxFromList (List <Integer> list){
       if (list == null || list.isEmpty()){
           return 0;
       }
//        Integer max = Integer.MIN_VALUE;
//        for (Integer n : list) {
//            if (n == null) {continue;}
//            if (n > max){
//                max = n;
//            }
//        }

       return list.stream()
                .filter(n -> n != null)
                .max(Comparator.naturalOrder()).orElse(0);
//        return max;
       }

    public List <String> sortAlphabetically (String s1, String s2, String s3){
        List <String> list = Arrays.asList(s1, s2, s3);
        return list.stream().filter(word -> word != null).sorted().collect(Collectors.toList());
    }

    public Boolean isPalindrome (String s){
        String[] split = s.split("");
        for (int i = 0; i < split.length/2; i++) {
            if (!split[i].equals(split[split.length-1-i])){
                return false;
            }
        }
        return true;
    }
}
