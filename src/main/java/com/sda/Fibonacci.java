package com.sda;

public class Fibonacci {
    public static void main(String[] args) {
        Fibonacci test = new Fibonacci();
        System.out.println(test.fibonacciElement(17));
//        System.out.println(test.fibonacciElement(5));
//        System.out.println(test.fibonacciElement(6));
    }

    public int fibonacciElement(int element) {
        int elem2 = 1;
        int elem1 = 1;
        int elem0 = 1;
        for (int i = 3; i <= element; i++) {
            elem0 = elem1 + elem2;
            elem2 = elem1;
            elem1 = elem0;
        }
        return elem0;

    }
}
