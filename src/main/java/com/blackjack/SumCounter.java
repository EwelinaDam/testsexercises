package com.blackjack;

import lombok.Getter;

public class SumCounter {

    @Getter
    private int sumUser;

    @Getter
    private int sumComp;


    public void sumCardsUser(int cardValue) {
        sumUser = sumUser + cardValue;

        }

    public void sumCardsComputer(int cardValue) {
        sumComp = sumComp + cardValue;

    }
    }

