package com.blackjack;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class CardGenerator {
    public final List<Integer> DECK = Arrays.asList(2, 3, 4, 5, 6, 7, 8, 9, 10, 2, 3, 4, 11);

    public int generate() {
        Random random = new Random();
        int indexCard = random.nextInt(13);
        return DECK.get(indexCard);
    }
}
